import unittest
from GeometricMethods import GeometricShapes


class TestTriangulo(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = GeometricShapes()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.areaTriangulo, 0)

    def test_areaTriangulo(self):
        self.geometric.areaTriangulo(10, 8)
        self.assertEqual(self.geometric.area, 40)

    def test_perimetroTriangulo(self):
        self.geometric.perimetroTriangulo(10, 8)
        self.assertEqual(round(self.geometric.perimetro), 30)

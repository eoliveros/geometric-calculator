import unittest
from GeometricMethods import GeometricShapes


class TestCirculo(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = GeometricShapes()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.areaCirculo, 0)

    def test_areaCirculo(self):
        self.geometric.areaCirculo(5)
        self.assertEqual(round(self.geometric.area), 78)

    def test_perimetroCirculo(self):
        self.geometric.perimetroCirculo(5)
        self.assertEqual((round(self.geometric.perimetro)), 31)

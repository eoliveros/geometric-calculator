import unittest
from GeometricMethods import GeometricShapes


class TestCuadrado(unittest.TestCase):

    def setUp(self) -> None:
        self.geometric = GeometricShapes()

    def test_intance(self):
        print("Instance")
        self.assertNotEqual(self.geometric, None)

    def test_default_value(self):
        print("test_default_value")
        self.assertNotEqual(self.geometric.areaCuadrado, 0)

    def test_areaCuadrado(self):
        self.geometric.areaCuadrado(4)
        self.assertEqual(self.geometric.area, 16)

    def test_perimetroCuadrado(self):
        self.geometric.perimetroCuadrado(4)
        self.assertEqual(self.geometric.perimetro, 16)

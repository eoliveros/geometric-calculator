from abc import ABCMeta, abstractmethod

class Figuras:
    __metaclass__ = ABCMeta

    @abstractmethod
    def area(self):
        raise NotImplementedError

    @abstractmethod
    def perimetro(self):
        raise NotImplementedError
import math
"""
Class with some calculator methods from different shapes.
@author: Emiliano Oliveros
"""
class GeometricMethods():

    def perimetros(self):
        perimetro = 0

    def areas(self):
        area = 0


class GeometricShapes():
    hipotenusa = 0
    radio = 0

    def areaCuadrado(self, base):
        self.area = base * base

    def areaTriangulo(self, base, altura):
     self.area = base*altura/2

    def areaCirculo(self, radio):
        self.area = math.pi * (radio*radio)

    def perimetroCuadrado(self, base):
        self.perimetro = int(base * 4)

    def perimetroTriangulo(self, base, altura):
        self.hipotenusa = math.sqrt(((base * base) + (altura * altura)))
        self.perimetro = base + altura + self.hipotenusa

    def perimetroCirculo(self, radio):
        self.perimetro = math.pi *2 *radio
